const path = require("path");
const webpack = require("webpack");

module.exports = {
  mode: 'development',
  entry: {
    app: "./src/index.js",
  },
  output: {
    filename: "[name].bundle.js",
    path: path.resolve(__dirname, "dist"),
    // clean: true
  },
  devServer: {
      port: 5001,
      static: path.resolve(__dirname, "dist"),
      hot: true,
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [
    // Enable the plugin
    new webpack.HotModuleReplacementPlugin(),
  ],
};