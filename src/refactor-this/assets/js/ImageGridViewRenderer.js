import '../css/main.css';
import { ImageDataGetter } from './ImageDataGetter';

export function ImageGridViewRenderer() { }

ImageGridViewRenderer.generateImages = function (page, fetchUrl, elementId) {
  let promises = []
  var currentPage = (page * 3) - 2

  while (currentPage <= (page * 3)) {
    promises.push(fetchUrl(currentPage));
    currentPage++
  }

  Promise.all(promises)
    .then(function (images) {
      for (var i = 0; i < images.length; i++) {
        for (var j = 0; j < images.length; j++) {
          document.getElementById(elementId).innerHTML +=
          '<div class="col" style="height: 400px; padding: 10px;">'
          + '  <img class="image" src="' + images[i][j].url + '" alt="' + images[i][j].name + '" style="height: 100%; object-fit: cover; width: 100%;" />'
          + '  <div class="middle">'
          + '    <a class="btn btn-dark" href="' + images[i][j].url + '" download="' + images[i][j].name + '">DOWNLOAD</a>'
          + '  </div>'
          + '</div>'
        }
      }
    })
}

ImageGridViewRenderer.prototype.render = function () {
  let nav =
    '<nav class="navbar navbar-expand-lg navbar-light bg-light">'
    + '  <a id="header-title" class="navbar-brand" href="#">Photo Sharing App</a>'
    + '  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">'
    + '    <span class="navbar-toggler-icon"></span>'
    + '  </button>'
    + '  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">'
    + '    <div class="navbar-nav">'
    + '      <a class="nav-link active" href="?nature">Nature <span class="sr-only">(current)</span></a>'
    + '      <a class="nav-link" href="?architecture">Architecture</a>'
    + '      <a class="nav-link" href="?fashion">Fashion</a>'
    + '    </div>'
    + '  </div>'
    + '</nav>'

  document.getElementById("main-view").innerHTML = nav

  var page = 1
  if (window.location.search.includes('page')) {
    page = Number(window.location.search.split('page=')[1])
  }

  if (window.location.search.includes('?nature')) {
    document.getElementById("main-view").innerHTML +=
      '<div class="container">'
      + '  <div id="nature-images" class="row row-cols-3"></div>'
      + '</div>'
      ImageGridViewRenderer.generateImages(page, ImageDataGetter.getNatureImagesFromPage, "nature-images")

  } else if (window.location.search.includes('?architecture')) {
    document.getElementById("main-view").innerHTML +=
      '<div class="container">'
      + '  <div id="architecture-images" class="row row-cols-3"></div>'
      + '</div>'
      ImageGridViewRenderer.generateImages(page, ImageDataGetter.getArchitectureImagesFromPage, "architecture-images")

  } else if (window.location.search.includes('?fashion')) {
    document.getElementById("main-view").innerHTML +=
      '<div class="container">'
      + '  <div id="fashion-images" class="row row-cols-3"></div>'
      + '</div>'
      ImageGridViewRenderer.generateImages(page, ImageDataGetter.getFashionImagesFromPage, "fashion-images")
  }

  var prevsearchstr = window.location.search.split('&page')[0] + '&page=' + (page - 1)
  var nextsearchstr = window.location.search.split('&page')[0] + '&page=' + (page + 1)
  var pagination =
    '<nav>'
    + '  <ul class="pagination">'
    + '    <li class="page-item"><a class="page-link" href="' + prevsearchstr + '">Previous</a></li>'
    + '    <li class="page-item"><a class="page-link" href="' + nextsearchstr + '">Next</a></li>'
    + '  </ul>'
    + '</nav>'

  document.getElementById("main-view").innerHTML += pagination
}
